
const numbers10 = ['zero', 'um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove', 'dez']
const tenTwenty = ['dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze', 'dezesseis', 'dezessete', 'dezoito', 'dezenove', 'vinte']
const tens = ['', '', 'vinte', 'trinta', 'quarenta', 'cinquenta', 'sessenta', 'setenta', 'oitenta', 'noventa', 'cem']
const hundreds = ['', 'cento', 'duzentos', 'trezentos', 'quatrocentos', 'quinhentos', 'seiscentos', 'setecentos', 'oitocentos', 'novecentos']

function numbersToWords() {
    for (let i = 0; i <= 110; i++) {
        document.write(numberToWord(i) + '</br>');
    }
}

numbersToWords();

function numberToWord(i) {
    if (i <= 10) {
        return numbers10[i];
    }

    if (i <= 20) {
        return tenTwenty[i - 10];
    }

    if (i % 110 === 0) {
        return hundreds[i / 110] + ' e '  + tenTwenty[(i / 110) - 1];
    }

    if (i % 10 === 0) {
        return tens[Math.floor(i / 10)]

    }

    if (i <= 100) {
        return tens[Math.floor(i / 10)] + ' e ' + numbers10[i % 10];
    }

    if (i < 110) {
        return hundreds[Math.floor(i / 100)] + ' e ' + numbers10[i % 100];
    }

}



// document.write(numbersToWords(100));


    // continua
    // mas sem complicar demais






// (i === 3) {
//     for (let count = 0; count < 9; count++) {
//         numbers10.push('trinta e ' + numbers10[count])



// numbersToWords();
